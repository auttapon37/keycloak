# Keycloak

## สามารถ Mount  volumn ได้ 2 แบบ

### Blind Mount ลิงค์ไฟล์เข้าสู่ Host โดยตรง สามารถเข้าถึงไฟล์จาก Directory ของ Host ได้เลย
รูปแบบการ Mount ไฟล์ Template
```
volumes:
      - ./themes/adminLTE/:/opt/jboss/keycloak/themes/adminLTE/
      ...
```
เมื่อแก้ไข Template เสร็จแล้วรันคำสั่ง
- docker-compose down
- docker-compose up -d

> `up อาจใช้เวลาสักพัก อาจจะต้อง Monitor ดูด้วย` (หรืออาจรันแบบ up เฉย ๆ ก็ได้)

```
docker-compose logs -f
```
### Volume สร้างระบบไฟล์ใหม่ นอกDocker container
รูปแบบการ Mount ไฟล์ Template
```
volumes:
      - keycloak_themes:/opt/jboss/keycloak/themes
```
เมื่อแก้ไข Template เสร็จแล้วรันคำสั่ง
- docker-compose down
- docker volume rm `keycloak_themes`
- docker volume create --name=`keycloak_themes`
- docker-compose up -d
- docker cp ./themes/. `keycloak_keycloak_1`:/opt/jboss/keycloak/themes
